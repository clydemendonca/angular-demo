import { ShoppingListRoutingModule } from './shopping-list-routing.module';
import { NgModule } from '@angular/core';

import { ShoppingListComponent } from './shopping-list.component';
import { AddShoppingListComponent } from './add-shopping-list/add-shopping-list.component';
import { ShoppingListDetailComponent } from './shopping-list-detail/shopping-list-detail.component';
import { CommonModule } from '@angular/common';
import { MyHttpService } from './myhttp.service';
import { HttpModule } from '@angular/http';

@NgModule({
    imports: [
        CommonModule,
        ShoppingListRoutingModule,
        HttpModule
    ],
    exports: [],
    declarations: [
        ShoppingListComponent,
        AddShoppingListComponent,
        ShoppingListDetailComponent
    ],
    providers: [
        MyHttpService
    ],
})
export class ShoppingListModule { }
