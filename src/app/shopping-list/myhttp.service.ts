
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MyHttpService {

    url = 'https://maps.googleapis.com/maps/api/geocode/json?address=161/1,%20MAHATMA%20GANDHI%20ROAD,%20ROOM%20NO.%2026,%20KOLKATA%20-%20700%20007&key=AIzaSyBPwSfLsY7GLL93iQraLwyUHNNHNYcUtRg';

    constructor(
        private http: Http
    ) { }

    getDataFromGoogle() {

        return this.http.get(this.url).map(
            (response : Response) => {
                return response.json();
            }
        );

    }

    
}