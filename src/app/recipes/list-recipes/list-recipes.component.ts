import { RecipeService } from './../recipe.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-recipes',
  templateUrl: './list-recipes.component.html',
  styleUrls: ['./list-recipes.component.css']
})
export class ListRecipesComponent implements OnInit {

  recipes=  [
    'Abc',
    'Def'
  ]

  constructor(
    private recipeService: RecipeService
  ) { }

  ngOnInit() {

    this.recipeService.recipeSubject.subscribe(
      (data: string) => {
        this.recipes.push(data);
      }
    );

  }

}
