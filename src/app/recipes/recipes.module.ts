import { RecipesRoutingModule } from './recipes-routing.module';

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RecipesComponent } from './recipes.component';
import { AddRecipeComponent } from './add-recipe/add-recipe.component';
import { ListRecipesComponent } from './list-recipes/list-recipes.component';
import { CommonModule } from '@angular/common';
import { RecipeService } from './recipe.service';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RecipesRoutingModule
    ],
    exports: [],
    declarations: [
        RecipesComponent,
        AddRecipeComponent,
        ListRecipesComponent,
    ],
    providers: [
        RecipeService
    ],
})
export class RecipesModule { }
