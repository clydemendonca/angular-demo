import { RecipesComponent } from './recipes.component';
import { ListRecipesComponent } from './list-recipes/list-recipes.component';
import { AddRecipeComponent } from './add-recipe/add-recipe.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        component: RecipesComponent,
        children: [
            { path: 'add', component: AddRecipeComponent },
            { path: 'list', component: ListRecipesComponent },
        ]
    },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RecipesRoutingModule { }
